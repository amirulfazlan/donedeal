<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

// register from website

Route::get('/register-agent', 'AgentController@create');
Route::post('/agents', 'AgentController@store');
Route::get('/register-talent', 'TalentController@create');
Route::post('talents', 'TalentController@store');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin-dashboard', function(){

    return view('admin-dashboard');
});

Route::get('/agent-dashboard', function(){

    return view('agent-dashboard');
});

Route::get('/talent-dashboard', function(){

    return view('talent-dashboard');
});



// Agent

Route::resource('agents', 'AgentController');

Route::resource('talents', 'TalentController');




