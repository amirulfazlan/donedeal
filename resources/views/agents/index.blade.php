<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <div class="m-grid m-grid--hor m-grid--root m-page">

        @include('layouts.header')

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            @include('layouts.sidebar')

            <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <h3 class="m-subheader__title m-subheader__title--separator">Agents</h3>
                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="#" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon la la-home"></i>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="" class="m-nav__link">
                                    <span class="m-nav__link-text">Agent Management</span>
                                </a>
                            </li>
                            <li class="m-nav__separator">-</li>
                            <li class="m-nav__item">
                                <a href="" class="m-nav__link">
                                    <span class="m-nav__link-text">Agent List</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                    <div>
                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
                            <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                <i class="la la-plus m--hide"></i>
                                <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="m-dropdown__wrapper">
                                <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                <div class="m-dropdown__inner">
                                    <div class="m-dropdown__body">
                                        <div class="m-dropdown__content">
                                            <ul class="m-nav">
                                                <li class="m-nav__section m-nav__section--first m--hide">
                                                    <span class="m-nav__section-text">Quick Actions</span>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-share"></i>
                                                        <span class="m-nav__link-text">Activity</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                        <span class="m-nav__link-text">Messages</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-info"></i>
                                                        <span class="m-nav__link-text">FAQ</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="" class="m-nav__link">
                                                        <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                        <span class="m-nav__link-text">Support</span>
                                                    </a>
                                                </li>
                                                <li class="m-nav__separator m-nav__separator--fit">
                                                </li>
                                                <li class="m-nav__item">
                                                    <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- END: Subheader -->
            <div class="m-content">
            {{-- Start Content --}}
            
            <div class="row">
                <div class="col-md-12">
                    								<!--begin::Portlet-->
								<div class="m-portlet m-portlet--success m-portlet--head-solid-bg"">
                                        <div class="m-portlet__head">
                                            <div class="m-portlet__head-caption">
                                                <div class="m-portlet__head-title">
                                                    <span class="m-portlet__head-icon">
                                                        <i class="flaticon-map-location"></i>
                                                    </span>
                                                    <h3 class="m-portlet__head-text">
                                                        List Of Registered Agent
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="m-portlet__head-tools">
                                                <ul class="m-portlet__nav">
                                                    <li class="m-portlet__nav-item">
                                                        <a href="" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                                                    </li>
                                                    <li class="m-portlet__nav-item">
                                                        <a href="" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-refresh"></i></a>
                                                    </li>
                                                    <li class="m-portlet__nav-item">
                                                        <a href="" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-circle"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="m-portlet__body">

                                            <div class="table-responsive">
                                                    <div class="table-scrollable">
                                                            <table class="table m-table m-table--head-bg-brand table table-bordered">
                                                                <thead>
                                                                        <style>
                                                                                #link { color: #ffffff; } /* CSS link color */
                                                                              </style>
                                                                    <tr>
                                                                            
                                                                            

                                                                            <th scope="col" width="15px">@sortablelink('id', 'No',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('name', 'Agent Name', ['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('email', 'E-Mail',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('phone_num', 'Phone Number',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('state', 'State',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('country', 'Country',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">@sortablelink('status', 'Status',['' => ''],  ['id' => 'link'])</th>
                                                                            <th scope="col" width="15px">Edit</th>
                                                                            <th scope="col" width="15px">Delete</th>
                                                                                
                                                                            
    

    
                                                                    </tr>
    
                                                                        
    
                                                                </thead>
                                                                <tbody>
    
                                                                   <tr>

                                                                        <?php $i = 0; ?>
                                                                        @foreach ($agents as $agent)
                                                                        <?php $i++; ?>

                                                                   <td>{{$i}}</td>
                                                                   <td>{{$agent->name}}</td>
                                                                   <td>{{$agent->email}}</td>
                                                                   <td>{{$agent->phone_num}}</td>
                                                                   <td>{{$agent->state}}</td>
                                                                   <td>{{$agent->country}}</td>
                                                                   <td>{{$agent->status}}</td>
                                                                   <td><a href="{{action('AgentController@edit', $agent->id)}}" class="btn btn-info disabled">Edit <span class="glyphicon glyphicon-pencil"></span></a></td>
                                                                       <td><form action="{{action('AgentController@destroy', $agent->id)}}" method="post">
                                                                            {{csrf_field()}}
                                                                            <input name="_method" type="hidden" value="DELETE">
                                                                           <button class="btn btn-danger" type="submit">Delete <span class="glyphicon glyphicon-trash"></span></button>
                                                                         </form></td>

                                                                       
                                                                   </tr>

                                                                   @endforeach
    
                                                                </tbody>
                                                            </table>
                                                    </div>

                                            </div>
                                               
                                        </div>
                                    </div>
    
                                    <!--end::Portlet-->

                </div>

            </div>
			


            </div>
			{{-- End Content --}}
			


            </div>

        </div>
		@include('layouts.footer')


	</div>

	@include('layouts.quicksidebar')

			<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

@include('layouts.scripts')
	
	


    
</body>


</html>