

<!--begin::Global Theme Bundle -->
<script src="{{ asset ('metronic/default/assets/vendors/base/vendors.bundle.js')}}" type="text/javascript"></script>
		
		<script src="{{ asset ('metronic/default/assets/demo/default/base/scripts.bundle.js')}}" type="text/javascript"></script>

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors -->
		<script src="{{asset('metronic/default/assets/vendors/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>

		<!--end::Page Vendors -->

		<!--begin::Page Scripts -->
		<script src="{{asset('metronic/default/assets/app/js/dashboard.js')}}" type="text/javascript"></script>

		<script src="{{asset('metronic/default/assets/demo/default/custom/crud/forms/widgets/select2.js')}}" type="text/javascript"></script>

		<script src="{{asset('metronic/default/assets/vendors/custom/datatables/datatables.bundle.js"')}}" type="text/javascript"></script>

		<script src="{{asset('metronic/default/assets/demo/default/custom/crud/metronic-datatable/base/data-local.js')}}" type="text/javascript"></script>

		



		<!--end::Page Scripts -->