<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <div class="m-grid m-grid--hor m-grid--root m-page">

        @include('layouts.header')

        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            @include('layouts.sidebar')

            <div class="m-grid__item m-grid__item--fluid m-wrapper">

            <!-- BEGIN: Subheader -->

									<!-- BEGIN: Subheader -->
					<div class="m-subheader ">
						<div class="d-flex align-items-center">
							<div class="mr-auto">
								<h3 class="m-subheader__title m-subheader__title--separator">Talent Registration Forms</h3>
								<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
									<li class="m-nav__item m-nav__item--home">
										<a href="#" class="m-nav__link m-nav__link--icon">
											<i class="m-nav__link-icon la la-home"></i>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Talents</span>
										</a>
									</li>
									<li class="m-nav__separator">-</li>
									<li class="m-nav__item">
										<a href="" class="m-nav__link">
											<span class="m-nav__link-text">Talent Registrations</span>
										</a>
									</li>

								</ul>
							</div>
							<div>
								<div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" m-dropdown-toggle="hover" aria-expanded="true">
									<a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
										<i class="la la-plus m--hide"></i>
										<i class="la la-ellipsis-h"></i>
									</a>
									<div class="m-dropdown__wrapper">
										<span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
										<div class="m-dropdown__inner">
											<div class="m-dropdown__body">
												<div class="m-dropdown__content">
													<ul class="m-nav">
														<li class="m-nav__section m-nav__section--first m--hide">
															<span class="m-nav__section-text">Quick Actions</span>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-share"></i>
																<span class="m-nav__link-text">Activity</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-chat-1"></i>
																<span class="m-nav__link-text">Messages</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-info"></i>
																<span class="m-nav__link-text">FAQ</span>
															</a>
														</li>
														<li class="m-nav__item">
															<a href="" class="m-nav__link">
																<i class="m-nav__link-icon flaticon-lifebuoy"></i>
																<span class="m-nav__link-text">Support</span>
															</a>
														</li>
														<li class="m-nav__separator m-nav__separator--fit">
														</li>
														<li class="m-nav__item">
															<a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

            <!-- END: Subheader -->
            <div class="m-content">
            {{-- Start Content --}}
            
            <div class="row">
                <div class="col-lg-12">

                    <div class="m-portlet">

                        <div class="m-portlet__head">

                                <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <span class="m-portlet__head-icon m--hide">
                                                <i class="la la-gear"></i>
                                            </span>
                                            <h3 class="m-portlet__head-text">
                                                Please Fill In All Details.
                                            </h3>
                                        </div>
                                    </div>

                        </div>

                        {{-- begin form --}}

                        <form method="post" action="{{url('talents')}}" enctype="multipart/form-data" class="m-form">
                            @csrf
                            <div class="m-portlet__body"">

                                <div class="m-form__section m-form__section--first">

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Full Name:</label>
                                                <input type="text" name="name" class="form-control m-input" placeholder="Enter full name">
                                                <span class="m-form__help">Please enter your full name</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">E-Mail:</label>
                                                <input type="email" name="email" class="form-control m-input" placeholder="e.g: michael@gmail.com">
                                                <span class="m-form__help">Please enter your e-Mail</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Password:</label>
                                                <input type="password" name="password" class="form-control m-input" placeholder="Password Must Be More Than 8 Character">
                                                <span class="m-form__help">Please enter your password</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Phone Number:</label>
                                                <input type="text" name="phone_number" class="form-control m-input" placeholder="e.g : 0123456789">
                                                <span class="m-form__help">Please enter your phone number</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Address Line 1:</label>
                                                <input type="text" name="address1" class="form-control m-input" >
                                                <span class="m-form__help">Please enter your address</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Address Line 2:</label>
                                                <input type="text" name="address2" class="form-control m-input">
                                                <span class="m-form__help">Please enter your address</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">City:</label>
                                                <input type="text" name="city" class="form-control m-input">
                                                <span class="m-form__help">Please enter your city</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Postcode:</label>
                                                <input type="number" name="postcode" class="form-control m-input">
                                                <span class="m-form__help">Please enter your postcode</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">State:</label>
                                                
                                                        <select class="form-control m-select2" id="state" name="state">
                                                            <option value=""></option>
                                                            <option value="Johor">Johor</option>
                                                            <option value="Melaka">Melaka</option>
                                                            <option value="Negeri Sembilan">Negeri Sembilan</option>
                                                            <option value="Kuala Lumpur">Kuala Lumpur</option>
                                                            <option value="Selangor">Selangor</option>
                                                            <option value="Perak">Perak</option>
                                                            <option value="Kedah">Kedah</option>
                                                            <option value="Perlis">Perlis</option>
                                                            <option value="Pahang">Pahang</option>
                                                            <option value="Kelantan">Kelantan</option>
                                                            <option value="Terengganu">Terengganu</option>
                                                            <option value="Pulau Pinang">Pulau Pinang</option>
                                                            <option value="Sabah">Sabah</option>
                                                            <option value="Sarawak">Sarawak</option>

                                                            
                                                        </select>
                                                    
                                                <span class="m-form__help">Please choose your state:</span>
                                        </div>

                                        <div class="form-group m-form__group">
                                                <label for="example_input_full_name">Country:</label>
                                                
                                                        <select class="form-control m-select2" id="country" name="country">
                                                            
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Singapore">Singapore</option>
                                                            <option value="Thailand">Thailand</option>
                                                            

                                                            
                                                        </select>
                                                    
                                                <span class="m-form__help">Please choose your state:</span>
                                        </div>

                                        <div class="form-group m-form__group">

                                                <label for="example_input_full_name">Specialty:</label><br>

                                                <div class="m-radio-inline">


                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="Singers"> Singers
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="Photographers"> Photographers
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="Sound Engineer"> Sound Engineer
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="MC"> MC
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="PA Systems"> PA Systems
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="1"> Baker
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Videographer
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Guitarist
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Designer
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Backup Vocal
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="1"> Dancer
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Clowns
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Catering
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Singing (Group)
                                                                
                                                                <span></span>
                                                        </label>

                                                        <label class="m-radio m-radio--solid">
                                                                <input type="radio" name="specialty" value="2"> Pianist
                                                                
                                                                <span></span>
                                                        </label>

                                                </div>

                                                <div class="m-radio-inline">


                                                       

                                                </div>

                                                <div class="m-radio-inline">




                                                </div>




                                                


                                                
                                       
                                                
                                                
                                                       
                                                    
                                                
                                        </div>



                                        







                                </div>

                            </div>

                            <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions m-form__actions">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-secondary">Cancel</button>
                                    </div>
                                </div>

                        </form>

                        {{-- end form --}}



                    </div>

                </div>

            </div>
            
            


            </div>
			{{-- End Content --}}
			


            </div>

        </div>
		@include('layouts.footer')


	</div>

	@include('layouts.quicksidebar')

			<!-- begin::Scroll Top -->
		<div id="m_scroll_top" class="m-scroll-top">
			<i class="la la-arrow-up"></i>
		</div>

		<!-- end::Scroll Top -->

@include('layouts.scripts')

<script>
    $(document).ready(function(){
        $('#state').select2({
            placeholder: "Select an option",
        });

    });

        $(document).ready(function(){
        $('#country').select2({
            placeholder: "Select an option",
        });

    });
</script>
	
	


    
</body>


</html>