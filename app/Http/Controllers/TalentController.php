<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Talent;
use Hash;
use DB;
use Auth;


class TalentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $talents = Talent::sortable()->paginate(5);
        return view('talent.index', compact('talents'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('talent.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $userinputpassword = $request->password;

        $users = new User([

            'name'          =>  $request->name,
            'email'         =>  $request->email,
            'password'      =>  Hash::make($userinputpassword),
            'user_roles'    =>  '3',    
            'status'        =>  'active'

        ]);

        $users->save();

        $latestuser = $users->id;

        $talents = new Talent([

            'user_id'       =>  $latestuser,
            'name'          =>  $users->name,
            'email'         =>  $users->email,
            'phone_num'     =>  $request->phone_number,
            'address1'      =>  $request->address1,
            'address2'      =>  $request->address2,
            'city'          =>  $request->city,
            'postcode'      =>  $request->postcode,
            'state'         =>  $request->state,
            'specialty'     =>  $request->specialty,
            'country'       =>  $request->country
        ]);

        $talents->save();

        if (Auth::check()) {

            return redirect('talents');



        }else {
            return redirect('login');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
