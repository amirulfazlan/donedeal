<?php

namespace App\Http\Controllers;

use App\Agent;
use App\User;
use Hash;
use DB;
use Datatables;

use Auth;

use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $agents = Agent::sortable()->paginate(10);
        return view('agents.index', compact('agents'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('agents.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $userinputpassword = $request->password;

        $users = new User([

            'name'          =>  $request->name,
            'email'         =>  $request->email,
            'password'      =>  Hash::make($userinputpassword),
            'user_roles'    =>  '2',    
            'status'        =>  'active'

        ]);

        $users->save();

        $latestuser = $users->id;
        
        
        $agents = new Agent([

            'user_id'       =>  $latestuser,
            'name'          =>  $users->name,
            'email'         =>  $users->email,
            'phone_num'     =>  $request->phone_number,
            'address1'      =>  $request->address1,
            'address2'      =>  $request->address2,
            'city'          =>  $request->city,
            'postcode'      =>  $request->postcode,
            'state'         =>  $request->state,
            'country'       =>  $request->country,
            'status'        =>  'active'
        ]);

        $agents->save();

        

        if (Auth::check()) {

            return redirect('agents');



        }else {
            return redirect('login');
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agent::destroy($id);
        \Session::flash('flash_message_delete','Agents successfully deleted.'); //<--FLASH MESSAGE
        return redirect('agents');
    }
}
