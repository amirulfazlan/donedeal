<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */


    // protected $redirectTo = '/dashboard';

    public function authenticated()
    {
        $user = Auth::user();
        // admin
        if ( $user->user_roles == 1 ) {
            return redirect('admin-dashboard');
        } 
        // agent
        elseif ($user->user_roles == 2) {
            return redirect('agent-dashboard');
        }
        // talent
        elseif ($user->user_roles == 3) {
            return redirect('talent-dashboard');
        }
        else {
            return redirect('home');
        }
    
        
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
