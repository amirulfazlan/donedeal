<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Agent extends Model
{
    use Sortable;

    public $sortable = [

        'id',
        'name',
        'email',
        'user_id',
        'phone_num',
        'address1',
        'address2',
        'city',
        'postcode',
        'state',
        'country',
        'status'
    ];
    protected $fillable = [


        'name',
        'email',
        'user_id',
        'phone_num',
        'address1',
        'address2',
        'city',
        'postcode',
        'state',
        'country',
        'status'
    ];
}
